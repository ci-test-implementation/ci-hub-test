# CI Hub Test

The purpose of this project is to be able to use a Docker container to run Unittests with Python.

To do this, we first created a `Dockerfile`, which is intended to build a custom-made Ubuntu 18.04 image to run Python scripts and Unittests.
Then we created the `.gitlab-ci.yml` file which has the task of defining the various `Jobs` of the `Pipeline`.

Each `push` runs the pipeline on GitLab.

FROM ubuntu:18.04

# Update
RUN apt-get update -y

# Install Essential Package
RUN apt install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev curl software-properties-common

# Install Python
RUN apt-get install python3 -y

# Install PIP
RUN apt install python3-pip -y && python3 -m pip install --upgrade pip --user

# Install Virtualenv
RUN pip3 install virtualenv

# Remove temp file
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#
CMD ["echo", "Hello from Hub Test!"]
